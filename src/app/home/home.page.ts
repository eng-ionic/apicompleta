import { Component } from '@angular/core';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  movieModel = {
    title: "",
    image: '',
    publishDate: ''
  }
  loading: boolean;

  constructor(
    private movieService: MovieService
  ) {}

  async saveMovie() {
    console.log(this.movieModel);
    // this.movieService.addMovie1(this.movieModel).subscribe(data => {
    //   console.log(data);
    // }, error => {
    //   console.log(error);
    // });

    // let progress = 0;

    // await this.movieService.addMovie(this.movieModel);

    // progress += 10;

    // await this.movieService.addMovie(this.movieModel);

    // progress += 100;

    // Promise.all([
    //   this.movieService.addMovie(this.movieModel),
    //   this.movieService.addMovie(this.movieModel),
    //   this.movieService.addMovie(this.movieModel)
    // ]).then(response => {
    //   response[2]
    // })
    this.loading = true;
    this.movieService.addMovie(this.movieModel)
      .then(data => {
        console.log(data);
      }).catch(error => {
        console.log(error);
      }).finally(() => {
        this.loading = false;
      });
  }

}
