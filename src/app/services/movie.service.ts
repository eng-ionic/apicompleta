import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(
    private http: HttpClient
  ) { }

  addMovie(movie) {
    return new Promise((resolve, reject) => {
      this.http.post("https://ab3025d95820.ngrok.io/movies", movie).subscribe(response => {
        resolve(response);
      }, error => {
        reject(error);
      });
    });
  }

  addMovie1(movie) {
    return this.http.post("https://ab3025d95820.ngrok.io/movies", movie);
  }

  getMovies() {
    return new Promise((resolve, reject) => {
      this.http.get('https://ab3025d95820.ngrok.io/movies').subscribe(response => {
        resolve(response);
      })
    });
  }
}
